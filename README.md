# lbnl (ELBY?) - Logic Block Notation Language

A language for writing digital logic circuits.

Context:
* Verilog and VHDL are TOO BIG!
* nMigen et al. are too programming-language-like

Wishes:
* "Do one thing and do it well"
* Purely synthesizable
* LL(1) grammars
* Transatable to verilog
* Fanatically minimalist

As I'm planning this language, I realize that I might as well strive to make it a textual equivalent of Logisim.



# Requirement axioms

Brainstorming concepts to include (cast the net WIDE):

  * Logic gates: AND, OR, NOT, etc
  * Wires (how 'bout them vectors?)
  * Modules (hear the echo of Alan Kay)
  * Input/Output pins/pads
  * (DE)MUX
  * ~~(EN|DE)CODER (no, just use normal gates?)~~
  * DFF (or other kinds?)
  * Latches
  * Comparator? (i.e. relational operators?)
  * Vectors and concatenation
  * Pull-up resistors

TODO just having NAND gates is going too far; define the intended middle ground

TODO consider C operators: Arighmetic, relational, logical, bitwise (no more).

TODO think of a circuit that tests many different elements.

TODO 1164, four-valued, or other?

TODO aim to be the ~~"scripting language", "rapid prototyping",~~ "agile language" for logic design?

TODO all number literals prefixed with {b, o, d, h}?

TODO `return` is usefull to avoid nesting depth.


## Adopting from Logisim?

  * __Wiring__
    * __Splitter__ (vector indices)
    * __Pin__ (all "ports" are inout)
    * ~~Probe (this is gui shit)~~
    * ~~Tunnel (wires have names)~~
    * ~~Pull-up resistor~~ (maybe later)
    * ~~Clock (this is for testbench, another language)~~
    * __Constant__ (TODO combine with enums?)
    * ~~Power (that is constant 1)~~
    * ~~Gnd (that is constant 0)~~
    * ~~Transistor (too low level?)~~
    * ~~Transmission gate (too low level)~~
    * __Bit extender__ (concatenation)
  * Gates
    * __NOT__
    * ~~Buffer (that's up to synthesis?)~~
    * __AND__
    * __OR__
    * ~~NAND (just not the result)~~
    * ~~NOR (just not the result)~~
    * __XOR__
    * ~~XNOR (not the result)~~
    * ~~Odd parity (std library?)~~
    * ~~Even parity (std library?)~~
    * ~~Controlled buffer (this will arise when someone codes it?)~~
    * ~~Controlled inverter (code it yourself?)~~
  * Plexers
    * __MUX__
    * ? DEMUX (depends on choice of "value" system)
    * Decoder (std library)
    * Priority encoder (std library)
    * ~~Bit selector (ain't this just a mux?)~~
  * Arithmetic (defer decision until vectors is settled?)
    * ? Adder
    * ? Subtractor
    * ? Multiplier
    * ? Divider
    * ? Negator
    * ? Comparator
    * ? Shifter
    * ? Bit adder
    * ? Bit finder
  * Memory
    * ~~D FF~~ (only one generic flip flop concept)
    * ~~T FF~~
    * ~~JK FF~~
    * ~~SR FF~~
    * Register (std library? @clk?)
    * Counter (std library?)
    * Shift register (std library?)
    * ~~Random generator (too high level)~~
    * RAM (std library?)
    * ROM (std library)
  * ~~Input/Output (these are hid peripherals)~~
  * ~~Base (these are gui tools)~~

TODO remove this list when everything is decided?

TODO consider having the most minimal concepts and make a "standard library"
which includes everything that is found in logisim.


## Operators

* ~~Unary (duplicates normal operators)~~
* __Arithmetic__ (+, -, *, /, %)
* Relational?
* ~~Logical (duplicates bitwise)~~
* __Bitwise__ (&, |, ~, ^)
  * ~~Shifting (duplicates concatenation?)~~
* Assignment
  * Blocking?
  * Non-blocking?
  * ~~Compound (duplicates normal assignment)~~
* ~~Ternary (duplicates other conditionals)~~



# Grammar (TODO and syntax?)

```
circuit    : 'circuit' name portlist body
name       : [a-zA-Z_]+
portlist   : '(' [name [, name]*] ')'
body       : '{' statementlist '}'
TODO etc
```



# Experimental sample circuits


## OR gate

```
circuit or(a, b, out) {
  out = a | b;
}
```


## 3-input AND

```
circuit and3(a, b, c, out) {
  ab = a & b;
  out = ab & c;
}
```


## Sequential circuit

```
circuit register(clk, a, out) {
  // TODO different keyword than 'dff'?
  dff (clk) {
    out = a;
  }
}
```


## Muxing

```
circuit conditionals(a, b, s, out) {
  mux (s) {
  case 0:
    out = a;
  case 1:
    out = b;
  }
}
```



# Sandbox

module (a, b, c, x, y) {
    case (a, b) {
    default:
        x := 0;
        y := 0;
    b00:
        case (c) {
        b0:
            y := 1;
        b1:
            x := 1;
        }
    b01:
        x := 1;
    }
}



module (a[7:0], b, c)



edge (a) {
}



sync (clk) # posedge
sync (!clk) # negedge



TODO one-hots?



dff a_o_valid (clk_i) := a_r_valid;
a_o_valid := reg(a_r_valid)@clk_i;
a_o_valid := a_r_valid @ clk_i;
x_o := (a & ~b) @ clk_i;
@clk_i {}
@clk_i x_o := (a & ~b);
@clk_i case (a, b) {...}
@(clk_i, ~rst) ...
sync (clk_i)


{
  needs (en, !wr, (data > 0));
  ...;
}
