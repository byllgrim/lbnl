(define
  (block-header name)

  (display "module ")
  (display name)
  (display " ();\n")
)

(define
  (block-footer)
  (display "endmodule\n")
)

(define
  (block name)
  (block-header name)
  (block-footer)
)

(block "empty")
