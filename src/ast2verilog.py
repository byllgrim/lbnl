#!/usr/bin/env python3

import json

if __name__ == "__main__":
    ast = json.loads(input()) # TODO multiline input?

    if ast[0] == "circuit":
        print("module", ast[1])

        if ast[2]:
            print(ast[2]) # TODO handle it properly
        print(";")

        if ast[3]:
            print(ast[3]) # TODO handle it properly

        print("endmodule")
    else:
        print("error: unrecognized ast node", ast[0])
