#!/usr/bin/env python3

import fileinput
import re

lexicals = [
    "^[a-zA-Z_]+",
    "\(",
    "\)",
    "{",
    "}"
];

def acceptmatch(match, line):
    span = match.span()
    token = match.string[span[0] : span[1]]
    print(token) # TODO other 'report' mechanisms to choose

    return line[span[1] :]

def parseline(line):
    done = False
    while not done:
        done = True
        line = line.strip()

        for rule in lexicals:
            match = re.search(rule, line)
            if match:
                line = acceptmatch(match, line)
                done = False
                break

    return line

def parseallinput():
    for line in fileinput.input():
        line = line.rstrip()
        line = parseline(line)
        if len(line) != 0:
            print("error: unknown lexeme '" + line + "'")
            quit()

if __name__ == "__main__":
    parseallinput()
