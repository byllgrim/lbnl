#!/usr/bin/env python3

# TODO this assumes the input is one token per line!!!!

import json

def parsekeyword(word):
    line = input()
    if line != word:
        print("error: expecting", word, "got", line)
        quit()

def parsebody():
    parsekeyword("{")
    # TODO list of statements
    parsekeyword("}")

    return [] # TODO populate ast

def parseportlist():
    parsekeyword("(")
    # TODO list of names
    parsekeyword(")")

    return [] # TODO populate ast

def parsecircuit(ast):
    parsekeyword("circuit")
    name = input() # TODO better checks
    portlist = parseportlist()
    body = parsebody()

    ast.append("circuit")
    ast.append(name)
    ast.append(portlist)
    ast.append(body)
    return ast # TODO don't take ast as argument?

if __name__ == "__main__":
    ast = []
    ast = parsecircuit(ast)
    print(json.dumps(ast)) # TODO sexpr instead of json?
