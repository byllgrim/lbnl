#! /usr/bin/env tclsh

proc printports {ports} {
  puts "("

  set first [lindex $ports 0]
  foreach p $ports {
    if {$p != $first} {
      puts -nonewline ","
    }
    puts [concat "inout wire" $p]
  }

  puts ");"
}

proc lb2verilog {b} {
  puts "module"
  puts [dict get $b name]

  printports [dict get $b ports]

  puts "endmodule"
}

proc readblock {chan} {
  set block ""
  while {[gets $chan line] >= 0} {
    set block [concat $block $line]
  }

  return $block
}

set block [readblock stdin]

lb2verilog $block
