#!/usr/bin/env tclsh

proc printports {ports} {
  puts "("

  set first [lindex $ports 0]
  foreach p $ports {
    if {$p != $first} {
      puts -nonewline ","
    }
    puts [concat "inout wire" $p]
  }

  puts ");"
}

proc printbody {body} {
  set lines [split $body "\n"]
  foreach line $lines {
    set trimmed [string trim $line]
    set quotelist [list "'" $trimmed "'"]
    puts [join $quotelist ""]
  }
}

proc block {name ports body} {
  puts [concat "module" $name]
  printports $ports
  printbody $body
  puts "endmodule"
}

block myblock {clk a b y} {
  sync clk {b <= a;}
  sync clk {y <= b;}
}
